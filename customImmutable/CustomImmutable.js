export default class CustomImmutable {

    constructor() {
        this.object = null
    }

    fromJS(object) {
        if (Object.entries(object).length > 0) {
            const res = []
            for (let i = 0; i < Object.entries(object).length; i++) {
                res.push(Object.entries(object)[i])
            }
            this.object = res
            return this
        }
        this.object = object
        return this
    }

    toJS() {
        const newObject = this.object
        const res = {}
        newObject.map(item => {
            res[item[0]] = item[1]
        })
        return res
    }

    setIn(fields, value) {
        const object = this.object
        for (let i = 0; i <= fields.length; i++) {
            for (let j = 0; j <= object.length; j++) {
                if (object[i][0] === fields[0]) {
                    object[i][1] = value
                }
            }
        }
        this.object = object
        return this
    }

}