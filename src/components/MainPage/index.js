import React, { Component } from 'react';
import { connect } from 'react-redux'
import TextField from '@material-ui/core/TextField';
import { updateUsers, changeEditable } from './actions'
import { Button } from "@material-ui/core"
import Edit from "./components/Edit"
import './style.css'

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newNom: "",
            newPrenom: "",
            newTel: "",
            search: ""
        }
        this.deleteFct = this.deleteFct.bind(this);
    }

    generateUsers(users) {
        const { edit } = this.state
        const res = []
        users.forEach(element => {
            res.push(userLine(element, this.deleteFct, this))
        });
        return res
    }


    deleteFct(user) {
        const { users } = this.props.mainPage
        const { dispatch } = this.props
        const res = users.filter(item => {
            return !(user.nom === item.nom && user.prenom === item.prenom)
        });
        dispatch(updateUsers(res))
    }

    render() {
        // testImmutable()
        if (!this.props.mainPage) return <div />
        const { users, editable } = this.props.mainPage
        const { dispatch } = this.props
        const { newPrenom, newNom, newTel, search, edit, toEdit } = this.state

        users.sort((a, b) => {
            if (a.nom < b.nom) {
                return -1
            }
        })

        let sortedUsers = users

        if (search !== "") {
            sortedUsers = users.filter(item => {
                return (item.nom.toLowerCase().includes(search) || item.prenom.toLowerCase().includes(search))
            });
        }
        if (editable) {
            return (<div className="container">
                <div className="contactList">
                    <TextField id="search" type="text" label=" search" value={search} onChange={e => this.setState({ search: e.target.value })}></TextField>
                    {this.generateUsers(sortedUsers)}
                </div>
                <div className="addContact">
                    <Edit user={toEdit} />
                </div>
            </div >
            );
        }
        return (<div className="container">
            <div className="contactList">
                <TextField id="search" type="text" label=" search" value={search} onChange={e => this.setState({ search: e.target.value })}></TextField>
                {this.generateUsers(sortedUsers)}
            </div>
            <div className="addContact">
                <h3>Add an user</h3>
                <TextField id="nom" type="text" label="nom" value={newNom} onChange={e => this.setState({ newNom: e.target.value })}></TextField>
                <TextField id="prenom" type="text" label="prenom" value={newPrenom} onChange={e => this.setState({ newPrenom: e.target.value })}></TextField>
                <TextField id="telephone" type="number" label="telephone" value={newTel} onChange={e => this.setState({ newTel: e.target.value })}></TextField>
                <Button style={{ marginTop: "20px" }} variant="outlined" color="primary" onClick={
                    () => {
                        if (newNom === "" || newPrenom === "") return true
                        dispatch(updateUsers([...users, { nom: newNom, prenom: newPrenom, tel: newTel }]))
                        this.setState({
                            newNom: "",
                            newPrenom: "",
                            newTel: ""
                        })
                    }
                }>
                    Add
                </Button>
            </div>
        </div >
        );
    }
}

export default connect((state) => state)(Home)


const userLine = (user, deleteFct, context) => {
    const { nom, prenom } = user
    const { dispatch, mainPage } = context.props
    return (
        <div className="contactLine" key={nom + prenom}>
            <div className="circle" style={{ backgroundColor: `#${intToRGB(hashCode(nom + prenom))}` }} > {`${nom.toUpperCase().charAt(0)}${prenom.toUpperCase().charAt(0)}`}</div>
            <div className="name">{`${prenom} ${nom}`}</div>
            <div>
                <Button variant="outlined" color="secondary" onClick={() => deleteFct({ nom, prenom })}>
                    Delete
            </Button>
                <Button variant="outlined" onClick={() => {
                    dispatch(changeEditable(!mainPage.editable))
                    context.setState({ toEdit: user })
                }}>
                    Edit
            </Button>
            </div>
        </div>
    )
}


function hashCode(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
}

function intToRGB(i) {
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
}