import CustomImmutable from '../../../../customImmutable/CustomImmutable'
import initialState from './initial_state'
import actionType from '../actions/actions-type'
import '../actions'

const immutable = new CustomImmutable()

// const updateUsers = (state, action) => fromJS(state)
//   .setIn(['users'], action.users)
//   .toJS()


const updateUsers = (state, action) => {
  return immutable.fromJS(state).setIn(['users'], action.users).toJS()
}

const changeUser = (state, action) => {
  const { users } = state
  const { userLog } = action
  const { newUser, oldUser } = userLog


  const newList = users.map(user => {
    if (oldUser.nom === user.nom && oldUser.prenom === user.prenom) {
      return newUser
    }
    return user
  })
  return immutable.fromJS(state).setIn(['users'], newList).toJS()
}


const changeEditable = (state, action) => (
  immutable.fromJS(state).setIn(['editable'], action.editable).toJS()
)

const mainPage = (state = initialState, action) => {
  switch (action.type) {
    case actionType.UPDATE_USERS:
      return updateUsers(state, action)
    case actionType.CHANGE_USER:
      return changeUser(state, action)
    case actionType.CHANGE_EDITABLE:
      return changeEditable(state, action)
    default:
      return state
  }
}
export default mainPage
