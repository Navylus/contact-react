import { connect } from 'react-redux'
import { changeUser, changeEditable } from '../../actions'
import React, { Component } from 'react';
import { TextField, Button } from "@material-ui/core"

class Edit extends Component {
    constructor(props) {
        super(props);
        const { user } = props
        const { nom, prenom, tel } = user
        this.state = {
            oldUser: user,
            newNom: nom,
            newPrenom: prenom,
            newTel: tel
        }
    }

    render() {
        const { newPrenom, newNom, newTel, oldUser } = this.state
        const { dispatch, mainPage } = this.props

        return (<div style={{ displa: "flex" }}>
            <h3>{`Edit ${oldUser.prenom}`}</h3>
            <TextField id="nom" type="text" label="nom" value={newNom} onChange={e => this.setState({ newNom: e.target.value })}></TextField>
            <TextField id="prenom" type="text" label="prenom" value={newPrenom} onChange={e => this.setState({ newPrenom: e.target.value })}></TextField>
            <TextField id="telephone" type="number" label="telephone" value={newTel} onChange={e => this.setState({ newTel: e.target.value })}></TextField>
            <Button style={{ marginTop: "20px" }} variant="outlined" color="primary" onClick={
                () => {
                    if (newNom === "" || newPrenom === "") return true
                    dispatch(changeUser({ newUser: { nom: newNom, prenom: newPrenom, tel: newTel }, oldUser }))
                    dispatch(changeEditable(!mainPage.editable))
                }
            }>
                Save Changes
                </Button>
        </div>);
    }
}

export default connect((state) => state)(Edit)