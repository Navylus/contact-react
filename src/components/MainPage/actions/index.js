import actionsType from './actions-type'

export const updateUsers = (users) => ({
    type: actionsType.UPDATE_USERS,
    users
})

export const changeUser = (userLog) => ({
    type: actionsType.CHANGE_USER,
    userLog
})

export const changeEditable = (editable) => ({
    type: actionsType.CHANGE_EDITABLE,
    editable
})
